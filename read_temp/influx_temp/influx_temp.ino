#include <Ethernet.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into pin 2 on the Arduino
#define ONE_WIRE_BUS 2
 
// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

EthernetClient client;

const IPAddress eth_default_ip(192, 168, 1, 99);
byte eth_mac[] = {0x90, 0xa2, 0xda, 0x00, 0x01, 0x4a};
const byte eth_server[] = {192, 168, 1, 119};
const int eth_port = 8086;
const int bufferSize = 256;
char buf[bufferSize] = {'\0'};
String temperatureStr;

bool eth_start(){
   Ethernet.begin(eth_mac, eth_default_ip);
   delay(2000); //delay to allow connection to be done
 
   //do a fast test if we can connect to server
   int conState = client.connect(eth_server, eth_port);
 
   if(conState > 0) {
    Serial.println("Connected to InfluxDB server");
    client.stop();
    return true;
   }
 
  //print the error number and return false
  Serial.print("Could not connect to InfluxDB Server, Error #");
  Serial.println(conState);
  return false;
}

void eth_send_data(char* data, int dataSize) {
  //first we need to connect to InfluxDB server   
  int conState = client.connect(eth_server, eth_port);
 
  if(conState <= 0) { //check if connection to server is stablished
    Serial.print("Could not connect to InfluxDB Server, Error #");
    Serial.println(conState);
    return;
  }
  
  //Send HTTP header and buffer
  client.println("POST /write?db=arduino HTTP/1.1");
  client.println("Host: www.arduino.com");
  client.println("User-Agent: Arduino/1.0");
  client.println("Connection: close");
  client.println("Content-Type: application/x-www-form-urlencoded");
  client.print("Content-Length: ");
  client.println(dataSize);
  client.println();
  client.println(data);
 
  delay(50); //wait for server to process data
 
  //Now we read what server has replied and then we close the connection
  Serial.println("Reply from InfluxDB");
  while(client.available()) { //receive char
    Serial.print((char)client.read());
  }  
  Serial.println(); //empty line
 
  client.stop();  
}

void setup() {
  //Serial interface for debugging purposes
  Serial.begin(9600);
  delay(1000);
  
  // Start up the library
  sensors.begin();
  
  eth_start(); 
}

void loop() {
    sensors.requestTemperatures(); // Send the command to get temperatures
    float temperature = sensors.getTempFByIndex(0);
    char temp_str[6];
    dtostrf(temperature, 4, 2, temp_str);
  
    //this will be number of chars written to buffer, return value of sprintf
    int numChars = 0;
    
    //First of all we need to add the name of measurement to beginning of the buffer
    numChars = sprintf(buf, "temperature_data,");

    //tag should have an space at the end 
    numChars += sprintf(&buf[numChars], "SOURCE=arduino_closet,PLACE=closet ");
    
    //after tags, comes the values!
    numChars += sprintf(&buf[numChars], "closet_temp=%s", temp_str);
 
    //Print the buffer on the serial line to see how it looks
    Serial.print("Sending following dataset to InfluxDB: ");
    Serial.println(buf);
 
    //send to InfluxDB
    eth_send_data(buf, numChars);
 
    //we have to reset the buffer at the end of loop
    memset(buf, '\0', bufferSize);
    delay(1000); //some small delay!  
}
